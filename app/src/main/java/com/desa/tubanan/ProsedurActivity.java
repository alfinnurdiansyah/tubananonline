package com.desa.tubanan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.desa.tubanan.Adapter.AdapterProsedur;
import com.desa.tubanan.item.itemprosedur;

import java.util.ArrayList;

public class ProsedurActivity extends AppCompatActivity {
    private NestedScrollView nested_scroll_view;
    private RecyclerView rvView;
    private ArrayList<itemprosedur> listitem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prosedur);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvView = findViewById(R.id.rv_prosedur);
        rvView.setLayoutManager(llm);
        nested_scroll_view = findViewById(R.id.nested_scroll_view);
        setTitle("Prosedur Pengajuan");
        listitem =new ArrayList<>();
        itemprosedur a=new itemprosedur();
        itemprosedur b=new itemprosedur();
        itemprosedur c=new itemprosedur();
        a.setIsi("Langkah-Langkahnya:\n 1. Pilih menu Ajukan surat\n 2. Masukkan Nik \n 3. Periksa data anda \n 4. Jika Benar klik tombol Ajukan \n 5. Pilih jenis surat \n 6. masukkan data data yang diperlukan \n 7. Klik Kirim dan tunggu");
        a.setJudul("Prosedur Pengajuan Surat");
        b.setIsi("Langkah-Langkahnya:\n 1. Pastikan data yang salah\n 2. Hubungi admin desa melalui e-lapor \n 3. Laporkan perbaikan data dengan melampirkan scan identitas diri (KTP/KK)");
        b.setJudul("Perbaikan Data Penduduk");
        c.setIsi("Jam kerja:\n Hari Senin-Jumat\n Jam 07.00-15.00 wib\n\nHubungi kami melalui surel:\n kontak@tubanan.id");
        c.setJudul("Kontak Desa");
        listitem.add(a);
        listitem.add(b);
        listitem.add(c);
        AdapterProsedur adapterProsedur= new AdapterProsedur(this,listitem,nested_scroll_view);
        rvView.setAdapter(adapterProsedur);

    }
}
