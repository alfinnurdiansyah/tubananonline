package com.desa.tubanan.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.desa.tubanan.R;
import com.desa.tubanan.item.itemstatus;

import java.util.ArrayList;

public class AdapterStatusSurat extends RecyclerView.Adapter<AdapterStatusSurat.ViewHolder>{
    private ArrayList<itemstatus> dataList;
    private Context context;
    public AdapterStatusSurat(Context context,ArrayList<itemstatus> data)
    {
        this.dataList = data;
        this.context = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_status_surat, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.No.setText(dataList.get(position).getId());
        holder.Jenis.setText(dataList.get(position).getJenissurat());
        holder.Status.setText(dataList.get(position).getStatussurat());
        holder.Tanggal.setText(dataList.get(position).getTanggalsurat());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView No,Jenis,Status,Tanggal;
        public ViewHolder(View itemView)
        {
            super(itemView);
            this.No=itemView.findViewById(R.id.nostatus);
            this.Jenis=itemView.findViewById(R.id.jenisstatus);
            this.Status=itemView.findViewById(R.id.statussurat);
            this.Tanggal=itemView.findViewById(R.id.tanggalstatus);
        }
    }
}
