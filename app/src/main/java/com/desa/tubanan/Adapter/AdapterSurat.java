package com.desa.tubanan.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.desa.tubanan.R;
import com.desa.tubanan.item.itemsurat;
import com.desa.tubanan.surat.SkBerkelakuanBaikActivity;
import com.desa.tubanan.surat.SkDomisiliActivity;
import com.desa.tubanan.surat.SkKepemilikanKendaraanBermotorActivity;
import com.desa.tubanan.surat.SkPerhiasan;
import com.desa.tubanan.surat.SkUsaha;
import com.desa.tubanan.surat.SuratKeteranganActivity;
import com.desa.tubanan.surat.SuratLaporHajatan;
import com.desa.tubanan.surat.SuratPengantarSkck;

import java.util.ArrayList;

public class AdapterSurat extends RecyclerView.Adapter<AdapterSurat.ViewHolder>{
    private ArrayList<itemsurat> dataList;
    Context context;
    public AdapterSurat(Context context,ArrayList<itemsurat> data)
    {
        this.dataList = data;
        this.context = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.itemsurat, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.Nama.setText(dataList.get(position).getNama());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dataList.get(position).getNama().equals("SURAT KETERANGAN")){
                    Intent intent=new Intent(context, SuratKeteranganActivity.class);
                    context.startActivity(intent);
                    finish();
                }else if(dataList.get(position).getNama().equals("SURAT KETERANGAN BERKELAKUAN BAIK")){
                    Intent intent=new Intent(context, SkBerkelakuanBaikActivity.class);
                    context.startActivity(intent);
                    finish();
                }else if(dataList.get(position).getNama().equals("SURAT KETERANGAN DOMISILI")){
                    Intent intent=new Intent(context, SkDomisiliActivity.class);
                    context.startActivity(intent);
                    finish();
                }else if(dataList.get(position).getNama().equals("SURAT KETERANGAN KEPEMILIKAN KENDARAAN BERMOTOR")){
                    Intent intent=new Intent(context, SkKepemilikanKendaraanBermotorActivity.class);
                    context.startActivity(intent);
                    finish();
                }else if(dataList.get(position).getNama().equals("SURAT KETERANGAN PERHIASAN")){
                    Intent intent=new Intent(context, SkPerhiasan.class);
                    context.startActivity(intent);
                    finish();
                }else if(dataList.get(position).getNama().equals("SURAT KETERANGAN USAHA")){
                    Intent intent=new Intent(context, SkUsaha.class);
                    context.startActivity(intent);
                    finish();
                }else if(dataList.get(position).getNama().equals("SURAT LAPOR HAJATAN")){
                    Intent intent=new Intent(context, SuratLaporHajatan.class);
                    context.startActivity(intent);
                    finish();
                }else if(dataList.get(position).getNama().equals("SURAT PENGANTAR SKCK")){
                    Intent intent=new Intent(context, SuratPengantarSkck.class);
                    context.startActivity(intent);
                    finish();
                }
            }
        });
    }
    private void finish(){
        if(context instanceof Activity){
            ((Activity)context).finish();
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView Nama;
        public ViewHolder(View itemView)
        {
            super(itemView);
            this.Nama=itemView.findViewById(R.id.textView4);
        }
    }
}
