package com.desa.tubanan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.desa.tubanan.Adapter.AdapterStatusSurat;
import com.desa.tubanan.item.itemdata;
import com.desa.tubanan.item.itemstatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private CardView webdesa,ajukansurat,Prosedur,elapor,about;
    private String alamatweb;
    private EditText cekstatussurat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Objects.requireNonNull(getSupportActionBar()).hide();
        alamatweb= getResources().getString(R.string.url);
        webdesa=findViewById(R.id.webdesa);
        about=findViewById(R.id.about);
        ajukansurat=findViewById(R.id.ajukansurat);
        Prosedur=findViewById(R.id.cardView2);
        elapor=findViewById(R.id.cardView3);
        cekstatussurat=findViewById(R.id.ceksurat);
        ajukansurat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CekNik();
            }
        });
        webdesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,WebDesaActivity.class);
                startActivity(intent);
            }
        });
        Prosedur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,ProsedurActivity.class);
                startActivity(intent);
            }
        });


        elapor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,ElaporActivity.class);
                startActivity(intent);
            }
        });
        cekstatussurat.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    ShowStatusSurat(cekstatussurat.getText().toString());
                    return true;
                }
                return false;
            }
        });
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,AboutActivity.class);
                startActivity(intent);
            }
        });
    }
    public void CekNik() {
        final Dialog myDialog = new Dialog(MainActivity.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        myDialog.setContentView(R.layout.ceknik);
        myDialog.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(myDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        final EditText nomornik=myDialog.findViewById(R.id.nomornik);
        myDialog.findViewById(R.id.btn_ceknik).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
                String url = alamatweb+"api/ceknik.php";
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                try {
                                    JSONObject jsonObject=new JSONObject(response);
                                    String n= jsonObject.getString("info");
                                    if(n.equals("success")){
                                        itemdata data =new itemdata();
                                        data.setNik(jsonObject.getJSONObject("result").getString("nik"));
                                        data.setAgama(jsonObject.getJSONObject("result").getString("agama"));
                                        data.setNama(jsonObject.getJSONObject("result").getString("nama"));
                                        data.setJk(jsonObject.getJSONObject("result").getString("jenis_kelamin"));
                                        data.setTtl(jsonObject.getJSONObject("result").getString("tempat_lahir")+","+jsonObject.getJSONObject("result").getString("tgl_lahir"));
                                        data.setPekerjaan(jsonObject.getJSONObject("result").getString("pekerjaan"));
                                        data.setAlamat(jsonObject.getJSONObject("result").getString("jalan"));
                                        data.setKewarganegaraan(jsonObject.getJSONObject("result").getString("kewarganegaraan"));
                                        ShowDetail(data);
                                    }else {
                                        Toast.makeText(MainActivity.this, "Nik Tidak Ada", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                myDialog.dismiss();
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Response", error.toString());
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("fnik", nomornik.getText().toString());
                        return params;
                    }
                };
                queue.add(postRequest);
            }
        });
        myDialog.show();
        myDialog.getWindow().setAttributes(lp);
    }
    public void ShowDetail(final itemdata data) {
        final Dialog myDialog = new Dialog(MainActivity.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        myDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        myDialog.setContentView(R.layout.layout_nik);
        myDialog.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(myDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        final TextView nomornik=myDialog.findViewById(R.id.nik);
        final TextView nama=myDialog.findViewById(R.id.namalengkap);
        final TextView jk=myDialog.findViewById(R.id.jeniskelamin);
        final TextView ttl=myDialog.findViewById(R.id.ttl);
        final TextView agama=myDialog.findViewById(R.id.agama);
        final TextView pekerjaan=myDialog.findViewById(R.id.pekerjaan);
        final TextView kewarganegaraan=myDialog.findViewById(R.id.kewarganegaraan);
        final TextView alamat=myDialog.findViewById(R.id.alamat);
        nomornik.setText(data.getNik());
        nama.setText(data.getNama());
        jk.setText(data.getJk());
        ttl.setText(data.getTtl());
        agama.setText(data.getAgama());
        pekerjaan.setText(data.getPekerjaan());
        kewarganegaraan.setText(data.getKewarganegaraan());
        alamat.setText(data.getAlamat());
        myDialog.findViewById(R.id.btnSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
                SharedPreferences getSharedPreferences = PreferenceManager
                        .getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor e = getSharedPreferences.edit();
                e.putString("nik", data.getNik());
                e.apply();
                Intent intent=new Intent(MainActivity.this,ListSuratActivity.class);
                startActivity(intent);
            }
        });
        myDialog.show();
        myDialog.getWindow().setAttributes(lp);
    }

    public void ShowStatusSurat(final String nik) {
        final Dialog myDialog = new Dialog(MainActivity.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        myDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        myDialog.setContentView(R.layout.layout_status_surat);
        myDialog.setCancelable(true);
        final RecyclerView rvView;

        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(myDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvView = myDialog.findViewById(R.id.rv_status_surat);
        rvView.setLayoutManager(llm);
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        String url = alamatweb+"api/cekstatussurat.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        ArrayList<itemstatus> listitem =new ArrayList<>();
                        itemstatus a=new itemstatus();
                        a.setId("No");
                        a.setJenissurat("Jenis Surat");
                        a.setTanggalsurat("Tanggal Surat");
                        a.setStatussurat("Status Surat");
                        listitem.add(a);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            String n= jsonObject.getString("info");
                            if(n.equals("success")){
                                JSONArray list=jsonObject.getJSONArray("data");
                                for(int i=0;i<list.length();i++){
                                    itemstatus b=new itemstatus();
                                    b.setStatussurat(list.getJSONObject(i).getString("status_surat"));
                                    b.setTanggalsurat(list.getJSONObject(i).getString("tanggal_surat"));
                                    b.setJenissurat(list.getJSONObject(i).getString("jenis_surat"));
                                    b.setId(String.valueOf(i+1));
                                    listitem.add(b);
                                }
                                AdapterStatusSurat adapterProsedur= new AdapterStatusSurat(MainActivity.this,listitem);
                                rvView.setAdapter(adapterProsedur);
                                myDialog.show();
                                myDialog.getWindow().setAttributes(lp);
                            }else {
                                Toast.makeText(MainActivity.this, "Nik Tidak Ada", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("fnik", nik);
                return params;
            }
        };
        queue.add(postRequest);
    }
}
