package com.desa.tubanan.surat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.desa.tubanan.ListSuratActivity;
import com.desa.tubanan.MainActivity;
import com.desa.tubanan.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SuratKeteranganActivity extends AppCompatActivity {
    EditText Keperluan_surat;
    Button btnKirim;
    String alamatweb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surat_keterangan);
        Keperluan_surat=findViewById(R.id.Keperluan_surat);
        btnKirim=findViewById(R.id.btnKirim);
        setTitle("Surat Keterangan");
        alamatweb= getResources().getString(R.string.url);
        final SharedPreferences getSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestQueue queue = Volley.newRequestQueue(SuratKeteranganActivity.this);
                String url = alamatweb+"surat/surat_keterangan/simpan-surat.php";
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                try {
                                    JSONObject jsonObject=new JSONObject(response);
                                    String n= jsonObject.getString("info");
                                    if(n.equals("success")){
                                        Toast.makeText(SuratKeteranganActivity.this, "Surat Berhasil Diajukan", Toast.LENGTH_LONG).show();
                                        Intent intent=new Intent(SuratKeteranganActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        SharedPreferences.Editor e = getSharedPreferences.edit();
                                        e.putString("nik", "");
                                        e.apply();
                                        finish();
                                    }else {
                                        Toast.makeText(SuratKeteranganActivity.this, "Surat Tidak Berhasil Diajukan", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Response", error.toString());
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("fnik",getSharedPreferences.getString("nik", ""));
                        params.put("fkeperluan",Keperluan_surat.getText().toString());
                        return params;
                    }
                };
                queue.add(postRequest);
            }
        });
    }
}
