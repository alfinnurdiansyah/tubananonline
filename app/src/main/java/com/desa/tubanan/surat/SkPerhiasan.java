package com.desa.tubanan.surat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.desa.tubanan.MainActivity;
import com.desa.tubanan.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SkPerhiasan extends AppCompatActivity {
    EditText NamaPerhiasan,Berat,TokoPerhiasan,LokasiTokoPerhiasan,KeperluanSurat;
    Button btnKirim;
    String alamatweb;
    Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sk_perhiasan);
        NamaPerhiasan=findViewById(R.id.nperhiasan);
        Berat=findViewById(R.id.bperhiasan);
        TokoPerhiasan=findViewById(R.id.tperhiasan);
        LokasiTokoPerhiasan=findViewById(R.id.lkperhiasan);
        KeperluanSurat=findViewById(R.id.Keperluan_surat);
        spinner= findViewById(R.id.jperhiasan);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.Jenis_Perhiasan, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        btnKirim=findViewById(R.id.btnKirim);
        setTitle("Surat Keterangan Perhiasan");
        alamatweb= getResources().getString(R.string.url);
        final SharedPreferences getSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestQueue queue = Volley.newRequestQueue(SkPerhiasan.this);
                String url = alamatweb+"surat/surat_keterangan_perhiasan/simpan-surat.php";
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                try {
                                    JSONObject jsonObject=new JSONObject(response);
                                    String n= jsonObject.getString("info");
                                    if(n.equals("success")){
                                        Toast.makeText(SkPerhiasan.this, "Surat Berhasil Diajukan", Toast.LENGTH_LONG).show();
                                        Intent intent=new Intent(SkPerhiasan.this, MainActivity.class);
                                        startActivity(intent);
                                        SharedPreferences.Editor e = getSharedPreferences.edit();
                                        e.putString("nik", "");
                                        e.apply();
                                        finish();
                                    }else {
                                        Toast.makeText(SkPerhiasan.this, "Surat Tidak Berhasil Diajukan", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Response", error.toString());
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("fnik",getSharedPreferences.getString("nik", ""));
                        params.put("fjenis_perhiasan",spinner.getSelectedItem().toString());
                        params.put("fnama_perhiasan",NamaPerhiasan.getText().toString());
                        params.put("fberat",Berat.getText().toString());
                        params.put("ftoko_perhiasan",TokoPerhiasan.getText().toString());
                        params.put("flokasi_toko_perhiasan",LokasiTokoPerhiasan.getText().toString());
                        params.put("fkeperluan",KeperluanSurat.getText().toString());
                        return params;
                    }
                };
                queue.add(postRequest);
            }
        });
    }
}
