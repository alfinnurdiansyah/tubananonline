package com.desa.tubanan.surat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.desa.tubanan.MainActivity;
import com.desa.tubanan.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SkKepemilikanKendaraanBermotorActivity extends AppCompatActivity {
    EditText Merk,Jenis,Tahun,CC,Warna,Rangka,Mesin,POLISI,bpkb,NamaPemilik,AlamatPemilik,KeperluanSurat;
    Button btnKirim;
    String alamatweb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sk_kepemilikan_kendaraan_bermotor);
        Merk=findViewById(R.id.Merk);
        Jenis=findViewById(R.id.Jenis);
        Tahun=findViewById(R.id.Tahun);
        CC=findViewById(R.id.CC);
        Warna=findViewById(R.id.Warna);
        Rangka=findViewById(R.id.Rangka);
        Mesin=findViewById(R.id.Mesin);
        POLISI=findViewById(R.id.Nopol);
        bpkb=findViewById(R.id.BPKB);
        NamaPemilik=findViewById(R.id.namapemilik);
        AlamatPemilik=findViewById(R.id.alamatpemilik);
        KeperluanSurat=findViewById(R.id.Keperluan_surat);
        btnKirim=findViewById(R.id.btnKirim);
        setTitle("Surat Keterangan Kepemilikan Kendaraan Bermotor");
        alamatweb= getResources().getString(R.string.url);
        final SharedPreferences getSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestQueue queue = Volley.newRequestQueue(SkKepemilikanKendaraanBermotorActivity.this);
                String url = alamatweb+"surat/surat_keterangan_kepemilikan_kendaraan_bermotor/simpan-surat.php";
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                try {
                                    JSONObject jsonObject=new JSONObject(response);
                                    String n= jsonObject.getString("info");
                                    if(n.equals("success")){
                                        Toast.makeText(SkKepemilikanKendaraanBermotorActivity.this, "Surat Berhasil Diajukan", Toast.LENGTH_LONG).show();
                                        Intent intent=new Intent(SkKepemilikanKendaraanBermotorActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        SharedPreferences.Editor e = getSharedPreferences.edit();
                                        e.putString("nik", "");
                                        e.apply();
                                        finish();
                                    }else {
                                        Toast.makeText(SkKepemilikanKendaraanBermotorActivity.this, "Surat Tidak Berhasil Diajukan", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Response", error.toString());
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("fnik",getSharedPreferences.getString("nik", ""));
                        params.put("fmerk_type",Merk.getText().toString());
                        params.put("fjenis_model",Jenis.getText().toString());
                        params.put("ftahun_pembuatan",Tahun.getText().toString());
                        params.put("fcc",CC.getText().toString());
                        params.put("fwarna_cat",Warna.getText().toString());
                        params.put("fno_rangka",Rangka.getText().toString());
                        params.put("fno_mesin",Mesin.getText().toString());
                        params.put("fno_polisi",POLISI.getText().toString());
                        params.put("fno_bpkb",bpkb.getText().toString());
                        params.put("fatas_nama_pemilik",NamaPemilik.getText().toString());
                        params.put("falamat_pemilik",AlamatPemilik.getText().toString());
                        params.put("fkeperluan",KeperluanSurat.getText().toString());
                        return params;
                    }
                };
                queue.add(postRequest);
            }
        });
    }
}
