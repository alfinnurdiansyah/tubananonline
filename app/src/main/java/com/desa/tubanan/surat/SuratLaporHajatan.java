package com.desa.tubanan.surat;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.desa.tubanan.MainActivity;
import com.desa.tubanan.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class SuratLaporHajatan extends AppCompatActivity {
    Calendar myCalendar;
    EditText Tanggal,JenisHiburan,PemilikHiburan,AlamatPemilikHiburan;
    Button btnKirim;
    String alamatweb;
    Spinner spinner,spinner1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surat_lapor_hajatan);
        myCalendar = Calendar.getInstance();
        Tanggal=findViewById(R.id.tanggal);
        JenisHiburan=findViewById(R.id.jhiburan);
        PemilikHiburan=findViewById(R.id.phiburan);
        AlamatPemilikHiburan=findViewById(R.id.APhiburan);
        btnKirim=findViewById(R.id.btnKirim);
        setTitle("Surat Lapor Hajatan");
        alamatweb= getResources().getString(R.string.url);
        final SharedPreferences getSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext());
        Tanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(SuratLaporHajatan.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, month);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        String formatTanggal = "dd-MM-yyyy";
                        SimpleDateFormat sdf = new SimpleDateFormat(formatTanggal);
                        Tanggal.setText(sdf.format(myCalendar.getTime()));
                    }
                },
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        spinner= findViewById(R.id.hari);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.hari, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner1 = findViewById(R.id.jhajat);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.jenis_hajat, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter1);
        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestQueue queue = Volley.newRequestQueue(SuratLaporHajatan.this);
                String url = alamatweb+"surat/surat_lapor_hajatan/simpan-surat.php";
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                try {
                                    JSONObject jsonObject=new JSONObject(response);
                                    String n= jsonObject.getString("info");
                                    if(n.equals("success")){
                                        Toast.makeText(SuratLaporHajatan.this, "Surat Berhasil Diajukan", Toast.LENGTH_LONG).show();
                                        Intent intent=new Intent(SuratLaporHajatan.this, MainActivity.class);
                                        startActivity(intent);
                                        SharedPreferences.Editor e = getSharedPreferences.edit();
                                        e.putString("nik", "");
                                        e.apply();
                                        finish();
                                    }else {
                                        Toast.makeText(SuratLaporHajatan.this, "Surat Tidak Berhasil Diajukan", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Response", error.toString());
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("fnik",getSharedPreferences.getString("nik", ""));
                        params.put("fjenis_hajat",spinner1.getSelectedItem().toString());
                        params.put("fhari",spinner.getSelectedItem().toString());
                        params.put("ftanggal",Tanggal.getText().toString());
                        params.put("fjenis_hiburan",JenisHiburan.getText().toString());
                        params.put("fpemilik",PemilikHiburan.getText().toString());
                        params.put("falamat_pemilik",AlamatPemilikHiburan.getText().toString());
                        return params;
                    }
                };
                queue.add(postRequest);
            }
        });
    }
}
