package com.desa.tubanan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.desa.tubanan.Adapter.AdapterSurat;
import com.desa.tubanan.item.itemsurat;

import java.util.ArrayList;
import java.util.Objects;

public class ListSuratActivity extends AppCompatActivity {
    private ArrayList<itemsurat> listsurat;
    private RecyclerView rvView;
    private AdapterSurat adapterSurat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_surat);
        setTitle("Pilihan Surat");
        rvView=findViewById(R.id.rv_list_surat);
        listsurat=new ArrayList<>();
        String[] arrData = {"SURAT KETERANGAN", "SURAT KETERANGAN BERKELAKUAN BAIK", "SURAT KETERANGAN DOMISILI", "SURAT KETERANGAN KEPEMILIKAN KENDARAAN BERMOTOR", "SURAT KETERANGAN PERHIASAN","SURAT KETERANGAN USAHA","SURAT LAPOR HAJATAN","SURAT PENGANTAR SKCK"};
        for(int i = 0; i< arrData.length; i++){
            itemsurat aa=new itemsurat();
            aa.setNama(arrData[i]);
            listsurat.add(aa);
        }
        adapterSurat=new AdapterSurat(ListSuratActivity.this,listsurat);
        rvView.setAdapter(adapterSurat);
    }
}
