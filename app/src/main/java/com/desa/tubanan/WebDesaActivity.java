package com.desa.tubanan;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class WebDesaActivity extends AppCompatActivity {
    private WebView webView;
    private ProgressBar progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_desa);

        WebView webView = (WebView) findViewById(R.id.webfaq);

        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);

        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);

        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        progress = (ProgressBar) findViewById(R.id.progressBar);
        progress.setMax(100);

        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("https://tubanan.id");
        WebDesaActivity.this.progress.setProgress(0);
    }

    public void setValue(int progress) {
        this.progress.setProgress(progress);
    }

    private class MyWebViewClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            WebDesaActivity.this.setValue(newProgress);
            super.onProgressChanged(view, newProgress);
        }
    }
}
