package com.desa.tubanan.item;

public class itemstatus {
    private String id,jenissurat,statussurat,tanggalsurat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJenissurat() {
        return jenissurat;
    }

    public void setJenissurat(String jenissurat) {
        this.jenissurat = jenissurat;
    }

    public String getStatussurat() {
        return statussurat;
    }

    public void setStatussurat(String statussurat) {
        this.statussurat = statussurat;
    }

    public String getTanggalsurat() {
        return tanggalsurat;
    }

    public void setTanggalsurat(String tanggalsurat) {
        this.tanggalsurat = tanggalsurat;
    }
}
