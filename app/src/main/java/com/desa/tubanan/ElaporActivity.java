package com.desa.tubanan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ElaporActivity extends AppCompatActivity implements View.OnClickListener {
    Button button1, button2, button3, button4, button5, button6, button7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elapor);
        setTitle("E-Lapor");
        button1=findViewById(R.id.polsek);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = "(0291) 7730110";
                Intent Call = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+number));
                startActivity(Call);
            }
        });
        button2=findViewById(R.id.rsud);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = "(0291) 591175";
                Intent Call = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+number));
                startActivity(Call);
            }
        });
        button3=findViewById(R.id.damkar);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = "(0291) 592113";
                Intent Call = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+number));
                startActivity(Call);
            }
        });
        button4=findViewById(R.id.listrik);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = "(0291) 591021";
                Intent Call = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+number));
                startActivity(Call);
            }
        });
        button5=findViewById(R.id.bpbd);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = "(0291) 598216";
                Intent Call = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+number));
                startActivity(Call);
            }
        });
        button6=findViewById(R.id.sar);
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = "(0291) 595941";
                Intent Call = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+number));
                startActivity(Call);
            }
        });
        button7=findViewById(R.id.wadesa);
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://wa.me/6285331108079"));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {
        String number = "085331108079";
        Intent Call = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+number));
        startActivity(Call);
    }

}
